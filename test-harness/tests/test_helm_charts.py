import os
import logging
import pytest
import time
import json
import yaml
from pytest_bdd import scenario, given, when, then, scenarios, parsers
from kubernetes import client, config, watch
from resources.helm import HelmAdapter

def pytest_configure():
    pytest.test_chart_name = []
    pytest.test_helm = None

@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"))
def k8s_cluster(kubeconfig):
    logging.info("loading kubeconfig " + kubeconfig)
    config.load_kube_config(kubeconfig)

@given(parsers.parse("resource quotas are set from {quotas_spec} and {limit_ranges_spec}"))
def apply_resource_quotas(test_namespace,quotas_spec, limit_ranges_spec):

    pytest.test_namespace = [test_namespace]
    api = client.CoreV1Api()

    with open(os.path.join(os.getcwd(),quotas_spec)) as f:
        quotas = yaml.safe_load(f)
    api.create_namespaced_resource_quota(namespace=test_namespace, body=quotas)

    logging.info("Created resource quota")

    with open(os.path.join(os.getcwd(),limit_ranges_spec)) as f:
        limit_ranges = yaml.safe_load(f)
    api.create_namespaced_limit_range(namespace=test_namespace, body=limit_ranges)


@when(parsers.parse("I install the chart at {url} with name {name} with values file {file}"))
def install_chart(url, name, test_namespace, file):
    if not hasattr(pytest, "test_chart_name"): 
        pytest_configure()
    pytest.test_helm = HelmAdapter(test_namespace)
    logging.info("Name: {}. Namespace: {}".format(name, test_namespace))

    api = client.ApiextensionsV1beta1Api()
    result = api.list_custom_resource_definition(field_selector="metadata.name=etcdclusters.etcd.database.coreos.com")

    items = result.items
    if len(items) > 0 and name == "etcd0":
        logging.info("CRD Found. Skipping installation")
        return

    logging.info("API Group: {}".format(result))
        
    pytest.test_chart_name.append(name)

    values = json.load(open(file, 'r'))
    logging.info("Installing " + name)
    logging.info("Helm Version: \n{}".format(os.popen("helm version").read()))
    logging.info("Helm Repo List:")
    logging.info(os.popen("helm repo list").read())
    logging.info("#########################################")
    logging.info(os.popen("helm list -n {} --filter {}".format(test_namespace, name)).read())
    pytest.test_helm.install(instance_name=name, chart_url=url, set_flag_values=values)
    logging.info("After helm.install:")
    logging.info("#########################################")
    logging.info(os.popen("helm list -n {} --filter {}".format(test_namespace, name)).read())
    
@then(parsers.parse("after {minutes} minutes all pods are running"))
def check_no_pods_waiting_or_evicted(minutes, test_namespace):
    v1 = client.CoreV1Api()
    logged_labels = [None]
    try:
        logging.info("list_namespaced_pod")
        start_time = time.time()
        while((time.time() - start_time) <= (float(minutes)*60)):
            # logging.info("**********************************************************")
            all_pod_running = True
            ret = v1.list_namespaced_pod(namespace=test_namespace)
            for item in ret.items:
                if not item.status.phase == 'Running':
                    # is it a job?
                    if(item.metadata.owner_references and len(item.metadata.owner_references) > 0 and item.metadata.owner_references[0].kind == 'Job'):
                        if logged_labels[-1] != item.metadata.labels:
                            logging.info("Job labels: {}".format(item.metadata.labels))
                            logged_labels.append(item.metadata.labels)
                        batch_api = client.BatchV1Api()
                        res = batch_api.read_namespaced_job(name=item.metadata.labels['job-name'], namespace=test_namespace)
                        if res.status.conditions and len(res.status.conditions) > 0 and not res.status.conditions[0].status:
                            all_pod_running = False
                            # logging.info("Job " + str(item.metadata.labels['job-name']) + " not ready  after " + str(int((time.time() - start_time))) + " seconds.")
                        continue
                    
                    # logging.info("Pod " + item.metadata.name + " not ready after " + str(int((time.time() - start_time))) + " seconds.")
                    all_pod_running = False
                    continue

                for status_container in item.status.container_statuses:
                    if not status_container.ready:
                        all_pod_running = False
                        # logging.info("Pod " + item.metadata.name + " not ready after " + str(int((time.time() - start_time))) + " seconds.")
                        break

            if(all_pod_running):
                logging.info("All pods are running after " + str(int((time.time() - start_time))) + " seconds.")
                break
        assert all_pod_running
    finally:
        logging.info("These charts will be uninstalled: \n{}".format(pytest.test_chart_name))
        for name in pytest.test_chart_name:
            logging.info("Uninstall chart " + name)
            pytest.test_helm.delete(name)
        # for namespace in pytest.test_namespace:
        #     logging.info("Delete namespace " + namespace)
        #     v1.delete_namespace(namespace)


scenarios('../features/helm_charts.feature')
