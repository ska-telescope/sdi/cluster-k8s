import logging
import pytest
import random
from pytest_bdd import scenario, given, when, then, scenarios, parsers
# from kubernetes.stream import stream
from kubernetes import client, config, watch

@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"))
def k8s_cluster(kubeconfig):
  logging.info("Loading kubeconfig " + kubeconfig)
  config.load_kube_config(kubeconfig)

@when(parsers.parse("I query all nodes details"))
def getNode():
  v1 = client.CoreV1Api()

  response = v1.list_node()
  dict_response = response.to_dict()
  nodeList = dict_response['items']

  pytest.nodeList = nodeList

@then(parsers.parse("the Condition {key} is {value}"))
def check_condition(key,value):

  for node in pytest.nodeList:
    conditions = node['status']['conditions']
    
    for condition in conditions:
      if condition['type'] == key:
        assert condition['status'] == value, node['metadata']['name'] + " " + key + " is not " + value
        logging.info("the Condition "+key+" passed for the node " + node['metadata']['name'])

@then(parsers.parse("the {key} is set on the {nodeType} Nodes"))
def check_taints(key,nodeType):


  for node in pytest.nodeList:
    
    try:
      node['metadata']['labels']['node-role.kubernetes.io/master']
    except KeyError as e:
      continue
    
    taints: dict = node['spec']['taints']

    assert taints, key + " is not set on " + node['metadata']['name']

    hasNoSchedule = False

    for taint in taints:
      
      if taint['effect'] == key:
        hasNoSchedule = True
  
    assert hasNoSchedule, node['metadata']['name'] + " " + key + " is set"
    logging.info("the "+key+" is set for " + node['metadata']['name'])

@then(parsers.parse("the {key} is not set on the {nodeType} Nodes"))
def check_no_taints(key,nodeType):

  for node in pytest.nodeList:
    
    try:
      node['metadata']['labels']['node-role.kubernetes.io/master']
    except KeyError as e:
      pass
    else:
      continue
    
    taints: dict = node['spec']['taints']

    assert not taints, key + " is not set on " + node['metadata']['name']

    if not taints:
      logging.info("the "+key+" is not set for " + node['metadata']['name'])
    else:
      for taint in taints:
        assert taint['effect'] != key, node['metadata']['name'] + " " + key + " is set"
        logging.info("the "+key+" is not set for " + node['metadata']['name'])

scenarios('../features/cluster.feature')