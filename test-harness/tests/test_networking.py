import time
import socket
from os import path
import yaml
import logging
import pytest
import requests
import socket
from bs4 import BeautifulSoup
from pytest_bdd import scenario, given, when, then, scenarios, parsers
from kubernetes import client, config, watch
from resources.delete_service import delete_service

@pytest.fixture
def ip_addr():
    return "127.0.0.1"

@pytest.fixture
def host_name():
    return "node"

@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"), target_fixture="ip_addr")
def k8s_cluster(kubeconfig,test_namespace):
    config.load_kube_config(kubeconfig)
    # Workaround from: https://github.com/kubernetes-client/python/issues/1284
    host = config.kube_config.Configuration.get_default_copy().host
    logging.info("HOST INFO: {}".format(host))
    logging.info("Host by Name: {}".format(socket.gethostbyname(host.split("//")[1].split(':')[0])))
    return socket.gethostbyname(host.split("//")[1].split(':')[0])

@when(parsers.parse("I create a deployment named <deployment_name> with nginx image on any worker"))
def deployment(deployment_name,test_namespace):
    container = client.V1Container(
        name=deployment_name,
        image="nginx",
        image_pull_policy="Always"
    )
    template = client.V1PodTemplateSpec(
        metadata=client.V1ObjectMeta(labels={"app": deployment_name}),
        spec=client.V1PodSpec(containers=[container])
    )
    deployment_spec = client.V1DeploymentSpec(
        replicas=1,
        template=template,
        selector={"matchLabels": {"app":deployment_name}}
    )
    deployment = client.V1Deployment(
        api_version="apps/v1",
        kind="Deployment",
        metadata=client.V1ObjectMeta(name=deployment_name,labels={"app": deployment_name}),
        spec=deployment_spec
    )
    k8s_apps_v1 = client.AppsV1Api()
    #logging.info("Test namespace: {}".format(test_namespace))
    resp_deploy = k8s_apps_v1.create_namespaced_deployment(body=deployment, namespace=test_namespace)
    logging.info("Deployment {} created".format(resp_deploy.metadata.name))
   
@when(parsers.parse("I expose the deployment with a service named <service_name>"))
def expose(deployment_name,service_name,test_namespace):
    svc = client.V1Service(
        api_version="v1",
        kind="Service",
        metadata=client.V1ObjectMeta(
            name=service_name,labels={"app": deployment_name}),
        spec=client.V1ServiceSpec(
            selector={"app": deployment_name},
            ports=[client.V1ServicePort(
                port=80,
                target_port=80,
                protocol="TCP"
                )
            ]
        )
    ) 
    # logging.info(svc)
    k8s_core_v1 = client.CoreV1Api()
    resp_svc = k8s_core_v1.create_namespaced_service(body=svc, namespace=test_namespace)
    logging.info("#############################################################")
    logging.info("Service {} created and exposing {} at IP address {}".format(resp_svc.metadata.name, resp_svc.spec.selector['app'], resp_svc.spec.cluster_ip))
    pytest.svc_ip_addr = resp_svc.spec.cluster_ip
    
@when(parsers.parse("I route traffic to the service <service_name> via an ingress of class <ingress_class> mounted at <ingress_path>"))
def route(service_name,ingress_class,ingress_path,test_namespace,ip_addr):
    ingress_spec = client.NetworkingV1beta1IngressSpec(
        rules=[client.NetworkingV1beta1IngressRule(
            host="test.haproxy.ingress",
            http=client.NetworkingV1beta1HTTPIngressRuleValue(
                paths=[client.NetworkingV1beta1HTTPIngressPath(
                    path=ingress_path,
                    backend=client.NetworkingV1beta1IngressBackend(
                        service_name=service_name,
                        service_port=80
                    )
                )]
            )
        )]
    )
    if ingress_class == "nginx":
        logging.info('Testing ip_addr last element: {}'.format(ip_addr.split('.')[-1]))
        if ip_addr.split('.')[-1] == '102': # syscore loadbalancer ip address TODO: import from inventory
            logging.info("Testing Syscore")
            annotations = {"kubernetes.io/ingress.class": "nginx",
                            "nginx.ingress.kubernetes.io/rewrite-target": "/"}
        else:
            logging.info("Testing Dev Cluster")
            annotations = {"kubernetes.io/ingress.class": "nginx-internal",
                            "nginx.ingress.kubernetes.io/rewrite-target": "/"}

    logging.info("Annotations: {}".format(annotations))

    ing = client.NetworkingV1beta1Ingress(
        api_version="networking.k8s.io/v1beta1",
        kind="Ingress",
        metadata=client.V1ObjectMeta(name="ingress-"+service_name, annotations=annotations),
        spec=ingress_spec
    )
    k8s_v1_beta1 = client.NetworkingV1beta1Api()
    resp_ing = k8s_v1_beta1.create_namespaced_ingress(namespace=test_namespace, body=ing)
    logging.info("#############################################################")
    # logging.info(resp_ing)
    logging.info("Ingress created with rules set for backend service {}".format(resp_ing.spec.rules[0].http.paths[0].backend.service_name))

@when(parsers.parse("I make a curl request to the HA Proxy of the cluster and get a response code not equal to 503"))
def curl_haproxy(ingress_class,deployment_name,ip_addr,ingress_path,test_namespace):
    if ingress_class == "nginx":
        ingress_path = ":80"+ingress_path

    url = "http://" + ip_addr + ingress_path
    apps_v1 = client.AppsV1Api()
    while True:
        resp = apps_v1.read_namespaced_deployment(namespace=test_namespace, name=deployment_name)
        repl = resp.status.ready_replicas
        if repl == 1:
            break

    counter = 5
    while counter>0:
        logging.info("URL: " + url)
        x = requests.get(url, headers = {"Host": "test.haproxy.ingress"})
        logging.info("Response code received: {}".format(x.status_code))
        if x.status_code == 200:
            break
        else:
            counter -= 1
            time.sleep(5-counter)
            continue

    soup = BeautifulSoup(x.text, features="lxml")
    pytest.test_page_title = soup.head.title.text

@then(parsers.parse("the response title has the words {title_expected}"))
def check_response(title_expected,test_namespace,deployment_name,service_name,ingress_class):
    try:
        assert pytest.test_page_title == title_expected
    finally:
        logging.info("Deleting...")
        client.NetworkingV1beta1Api().delete_namespaced_ingress(namespace=test_namespace,name="ingress-test-service-"+ingress_class)
        client.CoreV1Api().delete_namespaced_service(name=service_name,namespace=test_namespace)
        client.AppsV1Api().delete_namespaced_deployment(name=deployment_name,namespace=test_namespace)

@given(parsers.parse("more than one worker node"), target_fixture="host_name")
def get_worker_nodes():
    k8s = client.CoreV1Api()
    result = k8s.list_node(label_selector='!node-role.kubernetes.io/master')
    if len(result.items) > 1:
        logging.info("More than one node")
        pytest.pass_test = False
    else:
        pytest.pass_test = True
    logging.info("Name of first node: {}".format(result.items[0].metadata.labels['kubernetes.io/hostname']))
    return result.items[0].metadata.labels['kubernetes.io/hostname']

@when("the deployment is ready")
def check_deployment_readiness(test_namespace, deployment_name):
    apps_v1 = client.AppsV1Api()
    while True:
        resp = apps_v1.read_namespaced_deployment(namespace=test_namespace, name=deployment_name)
        repl = resp.status.ready_replicas
        if repl == 1:
            break
    logging.info("Replicas ready")
    time.sleep(3)

@when(parsers.parse("I make a curl request from a pod on a different worker to the service"))
def curl_with_job(host_name, deployment_name, test_namespace):
    # This section ensures that the pod running the job is not deployed on the same host as the deployment
    required_during_scheduling_ignored_during_execution = {
                            "labelSelector": {
                                "matchExpressions": [{
                                    "key": "app",
                                    "operator": "In",
                                    "values": [
                                        deployment_name
                                    ]
                                }]
                            },
                            "topologyKey": "kubernetes.io/hostname"
                        }
    pod_anti_affinity = client.V1PodAntiAffinity(required_during_scheduling_ignored_during_execution=[required_during_scheduling_ignored_during_execution])
    affinity = client.V1Affinity(pod_anti_affinity=pod_anti_affinity)
    # This is the part of the container that needed tweaking
    args = [ 
        "/bin/sh", 
        "-ec", 
        "curl -s -o /dev/null -w '%{http_code}' http://" + pytest.svc_ip_addr ]
    container = client.V1Container(args=args,name=deployment_name+"-test-job",image="buildpack-deps:curl")
    podspec = client.V1PodSpec(containers=[container], restart_policy='Never',affinity=affinity)
    template = client.V1PodTemplate()
    template.spec = client.V1PodTemplateSpec(spec=podspec)
    # Now create the Job object
    job = client.V1Job(api_version="batch/v1", kind="Job")
    job.metadata = client.V1ObjectMeta(namespace=test_namespace, name="curly")
    job.spec = client.V1JobSpec(template=template.spec)
    job_api = client.BatchV1Api()
    result = job_api.create_namespaced_job(namespace=test_namespace, body=job)
    logging.info("Job created: {}".format(result.spec))
    # logging.info("{}\n###################################################".format(client.CoreV1Api().list_namespaced_pod(namespace=test_namespace, label_selector="job-name=curly")))
    time.sleep(2)
    logging.info("{}\n###################################################".format(client.CoreV1Api().list_namespaced_pod(namespace=test_namespace, label_selector="job-name=curly")))

@then(parsers.parse("the http return code is 200"))
def check_return_code(service_name, deployment_name, test_namespace):
    api = client.CoreV1Api()
    for i in range(5):
        result = api.list_namespaced_pod(namespace=test_namespace, label_selector="job-name=curly")
        if result.items[0].status.phase == "Succeeded":
            break
        time.sleep(i)
    podname = result.items[0].metadata.name
    log = api.read_namespaced_pod_log(namespace=test_namespace, name=podname)
    try:
       assert pytest.pass_test or log == "200", "Status code not 200"
    finally:
        logging.info("Deleting...")
        client.BatchV1Api().delete_namespaced_job(namespace=test_namespace,name="curly")
        client.CoreV1Api().delete_namespaced_service(name=service_name,namespace=test_namespace)
        client.AppsV1Api().delete_namespaced_deployment(name=deployment_name,namespace=test_namespace)
        client.CoreV1Api().delete_namespaced_pod(namespace=test_namespace,name=podname)
        

scenarios('../features/networking.feature')
