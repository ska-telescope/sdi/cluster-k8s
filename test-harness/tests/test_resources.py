import os
import logging
import pytest
import random
import json
import yaml
from pytest_bdd import scenario, given, when, then, scenarios, parsers
# from kubernetes.stream import stream
from kubernetes import client, config, watch
from urllib3 import HTTPResponse


pytest.namespace = ""

@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"))
def k8s_cluster(kubeconfig):
  logging.info("Loading kubeconfig " + kubeconfig)
  config.load_kube_config(kubeconfig)

@given(parsers.parse("a Namespace {namespace}"))
def setNamespace(namespace):
  pytest.namespace = namespace

# @given(parsers.parse("default values for resource quotas and limits defined in {default_quotas_file}"))
# def load_default_values(default_quotas_file):
  
#   logging.info("path to file: {}".format(os.path.join(os.getcwd(), "..", default_quotas_file)))
#   with open(os.path.join(os.getcwd(), "..", default_quotas_file)) as file:
#     pytest.values_list = yaml.load(file, Loader=yaml.FullLoader)

#   logging.info(pytest.values_list)



@when(parsers.parse("I query the {kind}"))
def getInfo(kind):
  v1 = client.CoreV1Api()

  if(kind == 'ResourceQuotas'):
    # get Namespace ResourceQuotas
    response : HTTPResponse = v1.read_namespaced_resource_quota("compute-resources",pytest.namespace,_preload_content=False)
    pytest.quotas = json.loads(response.data.decode('utf-8'))

  elif(kind == 'LimitRanges'):
    # get Namespace LimitRanges    
    response : HTTPResponse = v1.read_namespaced_limit_range("limit-range",pytest.namespace,_preload_content=False)
    pytest.ranges = json.loads(response.data.decode('utf-8'))

@then(parsers.parse("the quota {quota} is set to {value}"))
def checkResourceQuotas(value,quota):
  hard = pytest.quotas['spec']['hard']

  assert hard[quota] == value, quota + " must have " + value + " of quota"


@then(parsers.parse("the range for type {resource} {quota} is set to {value}"))
def checkLimitRanges(resource,quota: str,value):
  limits = pytest.ranges['spec']['limits']

  nestedKeys = quota.split(".")

  for limit in limits:
    if limit['type'] == resource:
      limitValue = limit

      for key in nestedKeys:
        limitValue = limitValue[key]
      
      assert limitValue == value, quota + " must have " + value + " of range limit"
  
  
scenarios('../features/resources.feature')
