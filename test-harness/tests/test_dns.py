import time
from os import path
import yaml
import logging
import pytest
import requests
from pytest_bdd import scenario, given, when, then, scenarios, parsers
from kubernetes import client, config, watch
from kubernetes.stream import stream

pytest.podname = ""
pytest.resp = ""

@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"))
def k8s_cluster(kubeconfig, test_namespace):
    logging.info("loading kubeconfig " + kubeconfig)
    config.load_kube_config(kubeconfig)
    pytest.ns = test_namespace

@when(parsers.parse("I deploy a Pod with image {image} version {major}.{minor}.{patch} and name {podname}"))
def deploy_pod(image,major,minor,patch,podname):
    logging.info("Image: {}, Namespace: {}, Version: {}.{}.{}, Podname: {}".format(image,pytest.ns,major,minor,patch,podname))
    pod = client.V1Pod()
    pod.metadata = client.V1ObjectMeta(name=podname)
    container = client.V1Container(name=image)
    container.image=image + ":" + '.'.join([major,minor,patch])
    container.args=["sleep", "200"]
    spec = client.V1PodSpec(containers=[container])
    pod.spec = spec
    v1 = client.CoreV1Api()
    result = v1.create_namespaced_pod(namespace=pytest.ns, body=pod)
    pytest.podname = result.metadata.name
    all_good = False
    while True:
        resp = v1.read_namespaced_pod(name=pytest.podname, namespace=pytest.ns)
        if resp.status.phase != 'Running':
            continue

        for status_container in resp.status.container_statuses:
            if not status_container.ready:
                logging.info("Container status: Ready = {}".format(status_container.ready))
                continue
            else:
                all_good = True
                break

        if all_good:
            break

    logging.info("Pod deployed in {} namespace".format(resp.metadata.namespace))

@when(parsers.parse("I execute a DNS lookup for {url}")) 
def dns_lookup(url):
    exec_command = [
        '/bin/sh',
        '-c',
        'nslookup ' + url]
    v1 = client.CoreV1Api()
    logging.info("Executing nslookup {}...".format(url))
    resp = stream(v1.connect_get_namespaced_pod_exec, 
                   name = pytest.podname, 
                   namespace=pytest.ns,
                   command=exec_command,
                   stderr=True, stdout=True)
    pytest.resp = resp

@then(parsers.parse("I get a correctly resolving address"))
def is_correct_address():
    try:
        logging.info(pytest.resp)
        result = pytest.resp.split('\n')
        addr2 = result[5].split(': ')[0]
        nginx_ingress = result[4].split(' ')[3]
        assert addr2 == 'Address 2' or nginx_ingress == 'ingress-nginx-controller.ingress-nginx.svc.cluster.local', "Expected nslookup to include Address 2 or ingress-nginx-controller.ingress-nginx.svc.cluster.local, instead received:\n{}".format(pytest.resp)
    finally:
        v1 = client.CoreV1Api()
        v1.delete_namespaced_pod(name=pytest.podname, namespace=pytest.ns)
        logging.info("Deleted pod")

@when(parsers.parse("I deploy an all-blocking network policy"))
def block_all(test_namespace):
    with open(path.join(path.dirname(__file__), '../resources/allclosed.yaml')) as f:
        policy = yaml.safe_load(f)
        nw_v1 = client.NetworkingV1Api()
        resp_nwp = nw_v1.create_namespaced_network_policy(body=policy, namespace=test_namespace)
    logging.info("Policy blocking all traffic to and from {} namespace".format(resp_nwp.metadata.namespace))
    pytest.net_policy = resp_nwp.metadata.name

@when(parsers.parse("I switch to the sdp namespace"))
def switch_ns(test_namespace):
    ns = test_namespace+"-sdp"
    pytest.ns = ns
    v1 = client.CoreV1Api()
    ns_not_exists = True
    while True:
        namespaces = v1.list_namespace(field_selector='metadata.name='+ns).items
        if len(namespaces) > 0:
            logging.info("Namespace found: {}".format(ns))
            if namespaces[0].status.phase == 'Terminating':
                logging.info("Status.phase Terminating")
                continue
            else:
                ns_not_exists = False
                break
        else:
            logging.info("No existing namespaces found - creating...")
            break
        
    if ns_not_exists:
        v1.create_namespace(client.V1Namespace(metadata=client.V1ObjectMeta(name=ns)))
        time.sleep(1) # to avoid 500 error
    logging.info("Namespace {} exists".format(ns))

scenarios('../features/dns.feature')
