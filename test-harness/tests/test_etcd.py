import logging
import pytest
import random
from pytest_bdd import scenario, given, when, then, scenarios, parsers
from resources.etcdctl import EtcdCtlAdapter
from kubernetes.stream import stream
from kubernetes import client, config, watch

@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"))
def k8s_cluster(kubeconfig):
    logging.info("loading kubeconfig " + kubeconfig)
    config.load_kube_config(kubeconfig)

@when(parsers.parse("I create a pod with tolerations (so that it is executed on the master) with pv host path for having etcd certificates available"))
def create_pod(test_namespace):
    pod_manifest = {
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'etcd-client-tester'
        },
        'spec': {
            'nodeSelector':{
                'node-role.kubernetes.io/master': ''
            },
            'securityContext':{
                'runAsUser': 0
            },
            'containers': [{
                'name': 'container',
                'image': 'bitnami/etcd:latest',
                'command': ['sleep', '3600'],
                'env': [{
                    'name': 'ETCDCTL_API',
                    'value': '3'
                }],
                'volumeMounts': [{
                    'mountPath': '/var/lib/k8s/etcd',
                    'name': 'etcd-data'
                }, {
                    'mountPath': '/etc/kubernetes/pki/etcd',
                    'name': 'etcd-certs'
                }]
            }],
            'tolerations': [{
                'effect': 'NoExecute',
                'operator': 'Exists'
            },
            {
                'effect': 'NoSchedule',
                'operator': 'Exists'
            }],
            'volumes': [{
                'name':  'etcd-certs',
                'hostPath':{
                    'path': '/etc/kubernetes/pki/etcd',
                }
            },{
                'name': 'etcd-data',
                'hostPath': {
                    'path': '/var/lib/k8s/etcd',
                }
            }]
        }
    }
    v1 = client.CoreV1Api()
    logging.info("Creating etcd client pod")
    v1.create_namespaced_pod(test_namespace, pod_manifest)

def get_endpoint(test_namespace):
    v1 = client.CoreV1Api()
    ret = v1.list_namespaced_pod(test_namespace)
    while True:
        all_running = True
        for item in ret.items: 
            
            if not item.metadata.name == 'etcd-client-tester':
                continue

            if not item.status.phase == 'Running':
                all_running = False
                continue

            #logging.info(str(item.status))
            if item.status.container_statuses is not None: 
                for status_container in item.status.container_statuses:
                    if not status_container.ready:
                        all_running = False
                        break
            else:
                continue
            
            if(item.status.host_ip is not None):
                logging.info('Etcd endpoint is ' + item.status.host_ip + ':2379')
                return item.status.host_ip + ':2379'
            else:
                pytest.fail("Host IP not available")
            break

        if(all_running):
            break
        else:
            ret = v1.list_namespaced_pod(test_namespace)

@then(parsers.parse("I can list all the keys"))
def list_keys(test_namespace):
    exec_command = ['etcdctl', '--endpoints', get_endpoint(test_namespace), '--cacert', '/etc/kubernetes/pki/etcd/ca.crt', '--cert', '/etc/kubernetes/pki/etcd/healthcheck-client.crt', '--key', '/etc/kubernetes/pki/etcd/healthcheck-client.key', 'get', '/', '--prefix', '--keys-only']
    v1 = client.CoreV1Api()
    resp = stream(v1.connect_get_namespaced_pod_exec,
                  'etcd-client-tester',
                  test_namespace,
                  command=exec_command,
                  stderr=True, stdin=False,
                  stdout=True, tty=False)
    # logging.info(resp)
    lines = str(resp).split("\n\n")
    logging.info('Etcd has ' + str(len(lines)) + ' keys.')
    assert len(lines) > 1
    pytest.random_key = lines[random.randint(0, len(lines))]
    logging.info("Random key selected: " + pytest.random_key)

@then(parsers.parse("I can randomly read one of them"))
def read_random_key(test_namespace):
    exec_command = ['etcdctl', '--endpoints', get_endpoint(test_namespace), '--cacert', '/etc/kubernetes/pki/etcd/ca.crt', '--cert', '/etc/kubernetes/pki/etcd/healthcheck-client.crt', '--key', '/etc/kubernetes/pki/etcd/healthcheck-client.key', 'get', '--from-key', pytest.random_key]
    v1 = client.CoreV1Api()
    resp = stream(v1.connect_get_namespaced_pod_exec,
                  'etcd-client-tester',
                  test_namespace,
                  command=exec_command,
                  stderr=True, stdin=False,
                  stdout=True, tty=False)
    logging.info('Random key successully extracted.' )
    assert resp is not None

scenarios('../features/etcd.feature')
