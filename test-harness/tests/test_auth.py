import logging
import pytest
import resources
import base64
from time import sleep
from pytest_bdd import scenario, given, when, then, scenarios, parsers
from kubernetes import client, config, watch
from kubernetes.stream import stream


@given(parsers.parse("a Kubernetes cluster with KUBECONFIG {kubeconfig}"))
def k8s_cluster(kubeconfig):
    logging.info("loading kubeconfig " + kubeconfig)
    config.load_kube_config(kubeconfig)


@when(parsers.parse("I create a service account with name {name}"))
def create_sa(name, test_namespace):
    sa_manifest = {
        'apiVersion': 'v1',
        'kind': 'ServiceAccount',
        'metadata': {
            'name': name
        }
    }
    v1 = client.CoreV1Api()
    logging.info("Creating sa " + name)
    v1.create_namespaced_service_account(test_namespace, sa_manifest)
    if not hasattr(pytest, 'sa'):
        pytest.sa = []
    pytest.sa.append(name)


@when(parsers.parse("I create a Role with name {name} and apiGroups [{apigroups}], resources [{resources}], verbs [{verbs}]"))
def create_role(name, apigroups, resources, verbs, test_namespace):
    role_manifest = {
        'apiVersion': 'rbac.authorization.k8s.io/v1',
        'kind': 'Role',
        'metadata': {
            'name': name
        },
        'rules': [{
            'apiGroups': str(apigroups).split(','),
            'resources': str(resources).split(','),
            'verbs': str(verbs).split(',')
        }]
    }
    logging.info("creating role " + name)
    api = client.RbacAuthorizationV1Api()
    api.create_namespaced_role(test_namespace, role_manifest)
    if not hasattr(pytest, "role"):
        pytest.role = []
    pytest.role.append(name)


@when(parsers.parse("I create a RoleBinding with name {name} between service account {service_account} and role {role}"))
def creating_role_binding(name, service_account, role, test_namespace):
    rb_manifest = {
        'apiVersion': 'rbac.authorization.k8s.io/v1',
        'kind': 'RoleBinding',
        'metadata': {
            'name': name
        },
        'subjects': [{
            'kind': 'ServiceAccount',
            'name': service_account
        }],
        'roleRef': {
            'kind': 'Role',
            'name': role,
            'apiGroup': 'rbac.authorization.k8s.io'
        }
    }
    logging.info("creating role binding " + name)
    api = client.RbacAuthorizationV1Api()
    api.create_namespaced_role_binding(test_namespace, rb_manifest)
    if not hasattr(pytest, "rb"):
        pytest.rb = []
    pytest.rb.append(name)


@when(parsers.parse("I create a secret with name {name} and value {value}"))
def create_secret(name, value, test_namespace):
    secret_manifest = {
        'apiVersion': 'v1',
        'kind': 'Secret',
        'metadata': {
            'name': name
        },
        'data': {
            'value': base64.b64encode(bytes(value, 'utf-8')).decode('utf-8')
        }
    }
    v1 = client.CoreV1Api()
    logging.info("Creating secret " + name)
    v1.create_namespaced_secret(test_namespace, secret_manifest)
    if not hasattr(pytest, "secret"):
        pytest.secret = []
    pytest.secret.append(name)


@when(parsers.parse("I create a ConfigMap with name {name} and value {value}"))
def create_configmap(name, value, test_namespace):
    cm_manifest = {
        'apiVersion': 'v1',
        'kind': 'ConfigMap',
        'metadata': {
            'name': name
        },
        'data': {
            'value': value
        }
    }
    v1 = client.CoreV1Api()
    logging.info("Creating ConfigMap " + name)
    v1.create_namespaced_config_map(test_namespace, cm_manifest)
    if not hasattr(pytest, "cm"):
        pytest.cm = []
    pytest.cm.append(name)


@then(parsers.parse("a pod named {pod_name} with service account {service_account} {can} access the {resource} {resource_name}"))
def check_pod(pod_name, service_account, can, resource, resource_name, test_namespace):
    try:
        pod_manifest = {
            'apiVersion': 'v1',
            'kind': 'Pod',
            'metadata': {
                'name': pod_name
            },
            'spec': {
                'serviceAccountName': service_account,
                'containers': [{
                    'name': 'container',
                    'image': 'bitnami/kubectl:latest',
                    'command': ['sleep', '3600']
                }]
            }
        }
        exec_and_wait(pod_name, pod_manifest, can, [
                      'kubectl', 'get', resource, resource_name], test_namespace)
    finally:
        final_clean(pod_name, test_namespace)


def exec_and_wait(pod_name, pod_manifest, can, exec_command, test_namespace):
    v1 = client.CoreV1Api()
    logging.info("Creating pod " + pod_name)
    v1.create_namespaced_pod(test_namespace, pod_manifest)
    logging.info("check access...")
    ret = v1.list_namespaced_pod(test_namespace)
    while True:
        all_running = True
        for item in ret.items:
            if item.metadata.name == pod_name:
                if not item.status.phase == 'Succeeded' and not item.status.phase == 'Running':
                    all_running = False
                    break

                if item.status.container_statuses is not None:
                    for status_container in item.status.container_statuses:
                        if not status_container.ready:
                            all_running = False
                            break
                else:
                    all_running = False

                break

        if(all_running):
            break
        else:
            ret = v1.list_namespaced_pod(test_namespace)

    resp = stream(v1.connect_get_namespaced_pod_exec,
                  pod_name,
                  test_namespace,
                  command=exec_command,
                  stderr=True,
                  stdin=False,
                  stdout=True,
                  tty=False,
                  _preload_content=True)
    if(can == 'can'):
        assert resp is not None
    elif can == 'cannot':
        assert 'Forbidden' in resp
    else:
        pytest.fail("Verb " + can + " not valid.")


def final_clean(pod_name, test_namespace):
    v1 = client.CoreV1Api()
    api = client.RbacAuthorizationV1Api()
    for role in pytest.role:
        logging.info("deleting role " + role)
        api.delete_namespaced_role(role, test_namespace)
    for sa in pytest.sa:
        logging.info("deleting sa " + sa)
        v1.delete_namespaced_service_account(sa, test_namespace)
    for rb in pytest.rb:
        logging.info("deleting rb " + rb)
        api.delete_namespaced_role_binding(rb, test_namespace)
    if hasattr(pytest, "cm"):
        for cm in pytest.cm:
            logging.info("deleting secret " + cm)
            v1.delete_namespaced_config_map(cm, test_namespace)
    if hasattr(pytest, "secret"):
        for secret in pytest.secret:
            logging.info("deleting secret " + secret)
            v1.delete_namespaced_secret(secret, test_namespace)
    v1.delete_namespaced_pod(pod_name, test_namespace)

    pytest.role = []
    pytest.sa = []
    pytest.rb = []
    pytest.secret = []
    pytest.cm = []


scenarios('../features/auth.feature')
