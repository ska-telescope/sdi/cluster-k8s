@auth
Feature: RBAC
    Create Service account, role and role binding

    Scenario: role and role binding
        Given a Kubernetes cluster with KUBECONFIG .kube/config
        When I create a service account with name mysa
        And I create a Role with name myrole and apiGroups [""], resources [secrets], verbs [get,list]
        And I create a RoleBinding with name myrolebinding between service account mysa and role myrole
        And I create a secret with name mysecret and value pippo
        And I create a ConfigMap with name configmap and value pippo
        Then a pod named test-pod with service account mysa can access the secret mysecret

    Scenario: role and role binding - sad path
        Given a Kubernetes cluster with KUBECONFIG .kube/config
        When I create a service account with name mysa2
        And I create a Role with name myrole2 and apiGroups [""], resources [configmaps], verbs [get,list]
        And I create a RoleBinding with name myrolebinding2 between service account mysa2 and role myrole2
        And I create a secret with name mysecret2 and value pippo2
        Then a pod named test-pod2 with service account mysa2 cannot access the secret mysecret2

    Scenario: role and role binding with config maps
        Given a Kubernetes cluster with KUBECONFIG .kube/config
        When I create a service account with name mysa3
        And I create a Role with name myrole3 and apiGroups [""], resources [configmaps], verbs [get,list]
        And I create a RoleBinding with name myrolebinding3 between service account mysa3 and role myrole3
        And I create a ConfigMap with name configmap and value pippo
        Then a pod named test-pod3 with service account mysa3 can access the ConfigMap configmap

    Scenario: role and role binding with config maps - sad path
        Given a Kubernetes cluster with KUBECONFIG .kube/config
        When I create a service account with name mysa4
        And I create a Role with name myrole4 and apiGroups [""], resources [pods], verbs [get,list]
        And I create a RoleBinding with name myrolebinding4 between service account mysa4 and role myrole4
        And I create a ConfigMap with name configmap2 and value pippo2
        Then a pod named test-pod4 with service account mysa4 cannot access the ConfigMap configmap2