Feature: DNS

Scenario: Test Internal DNS lookup
    Given a Kubernetes cluster with KUBECONFIG .kube/config 
    When I deploy a Pod with image busybox version 1.28.3 and name busybox-internal-dns-lookup
    And I execute a DNS lookup for ingress-nginx-controller.ingress-nginx
    Then I get a correctly resolving address

Scenario: Test External DNS lookup
    Given a Kubernetes cluster with KUBECONFIG .kube/config 
    When I deploy a Pod with image busybox version 1.28.3 and name busybox-external-dns-lookup
    And I execute a DNS lookup for google.com
    Then I get a correctly resolving address

@skip
Scenario: Test Network Policy blocking Egress in CI namespace does not impede default namespace
    Given a Kubernetes cluster with KUBECONFIG .kube/config 
    When I deploy an all-blocking network policy
    And I switch to the sdp namespace
    And I deploy a Pod with image busybox version 1.28.3 and name busybox-dns-lookup-nwp-test
    And I execute a DNS lookup for google.com
    Then I get a correctly resolving address
