Feature: ETCD
    Get key from k8s etcd

Scenario: Get key from k8s etcd
    Given a Kubernetes cluster with KUBECONFIG .kube/config
    When I create a pod with tolerations (so that it is executed on the master) with pv host path for having etcd certificates available
    Then I can list all the keys 
    And I can randomly read one of them