Feature: Helm Charts
    Install Umbrella Helm chart and smoke test it

Scenario: Install tango-example and smoke test it
    Given a Kubernetes cluster with KUBECONFIG .kube/config
    And resource quotas are set from ./resources/namespace_quotas.yaml and ./resources/limit_ranges.yaml
    When I install the chart at https://artefact.skao.int/repository/helm-internal/ska-tango-examples-0.2.12.tgz with name test2 with values file resources/skampi_values.json
    Then after 10 minutes all pods are running 
