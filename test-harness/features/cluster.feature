# 3. Is the NoSchedule taint set on the master nodes
# [Dalmiro]
Feature: Cluster Taints & Health

Background:
    Given a Kubernetes cluster with KUBECONFIG .kube/config 

Scenario: Check Master Node Taints
    When I query all nodes details
    Then the NoSchedule is set on the master Nodes
    Then  the NoSchedule is not set on the worker Nodes

Scenario: Check Node Health Status
    When I query all nodes details
    Then the Condition Ready is True
    And  the Condition NetworkUnavailable is False
    And  the Condition MemoryPressure is False
    And  the Condition DiskPressure is False
    And  the Condition PIDPressure is False
