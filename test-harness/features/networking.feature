Feature: Networking
    Test ingress and inter-pod communication

Background: Cluster
    Given a Kubernetes cluster with KUBECONFIG .kube/config

Scenario: Create, expose and curl service via Ingress
    When I create a deployment named <deployment_name> with nginx image on any worker
    And I expose the deployment with a service named <service_name>
    And I route traffic to the service <service_name> via an ingress of class <ingress_class> mounted at <ingress_path>
    And I make a curl request to the HA Proxy of the cluster and get a response code not equal to 503
    Then the response title has the words Welcome to nginx!

Examples:
    | ingress_class | deployment_name   | ingress_path       | service_name         |
    | nginx         | test-depl-nginx   | /test-path-nginx   | test-service-nginx   |

Scenario: Check cross-node communication
    Given more than one worker node
    When I create a deployment named <deployment_name> with nginx image on any worker
    And I expose the deployment with a service named <service_name>
    And the deployment is ready
    And I make a curl request from a pod on a different worker to the service
    Then the http return code is 200
    
Examples:
    | deployment_name | service_name   |
    | test-job-nginx  | test-job-nginx |

