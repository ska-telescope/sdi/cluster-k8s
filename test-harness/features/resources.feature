Feature: Resource Management

@xfail
Scenario: Check ResourceQuotas and LimitRanges
    Given a Kubernetes cluster with KUBECONFIG .kube/config
    And a Namespace integration
    When I query the ResourceQuotas
    When I query the LimitRanges
    Then the quota requests.cpu is set to 16
    And  the quota requests.memory is set to 16Gi
    And  the quota requests.ephemeral-storage is set to 15Gi
    And  the quota limits.cpu is set to 16
    And  the quota limits.memory is set to 16Gi
    And  the quota limits.ephemeral-storage is set to 20Gi
    And  the range for type Container default.memory is set to 128Mi
    And  the range for type Container default.cpu is set to 100m
    And  the range for type Container defaultRequest.memory is set to 128Mi
    And  the range for type Container defaultRequest.cpu is set to 100m
