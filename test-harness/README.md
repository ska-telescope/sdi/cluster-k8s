Cluster-k8s basic testing 
=========================
BDD style tests are created that test the correct functioning of:

* Pods can be scheduled on each worker
* DNS - internal, namespace, external
* Storage allocation, access, and tidy up - nfs/block
* deployment of calico on all nodes
* application of NoSchedule taints on masters
