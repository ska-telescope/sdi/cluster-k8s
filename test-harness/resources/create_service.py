# Copyright 2016 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from os import path
import time
from bs4 import BeautifulSoup
import yaml

from kubernetes import client, config, watch


def create_service():
    # Configs can be set in Configuration class directly or using helper
    # utility. If no argument provided, the config will be loaded from
    # default location.
    config.load_kube_config('/home/aragorn/SKA/code/glr-kubeconfig')

    with open(path.join(path.dirname(__file__), "nginx.yaml")) as f:
        dep = yaml.safe_load(f)
        k8s_apps_v1 = client.AppsV1Api()
        resp_deploy = k8s_apps_v1.create_namespaced_deployment(
            body=dep, namespace="default")
    # print(resp_deploy)

    with open(path.join(path.dirname(__file__), "nginx_svc.yaml")) as f:
        svc = yaml.safe_load(f)
        k8s_core_v1 = client.CoreV1Api()
        resp_svc = k8s_core_v1.create_namespaced_service(
            body=svc, namespace="default"
        )
    # print(resp_svc)
    
    with open(path.join(path.dirname(__file__), "ingress.yaml")) as f:
        ing = yaml.safe_load(f)
        k8s_v1_beta1 = client.NetworkingV1beta1Api()
        resp_ing = k8s_v1_beta1.create_namespaced_ingress(namespace="default", body=ing)
    # print(resp_ing)

    # time.sleep(1)

    # print(resp_deploy)
    # print(resp_svc)
    # print(resp_ing)
    print("Deployment created. status='%s'" % resp_deploy.status)
    # print("IP Address: %s" % cluster_ip)
    # # return resp_deploy.metadata.name, resp_svc.spec.cluster_ip, resp_ing.spec.rules[1].http.paths[0].backend.service_name
    return resp_deploy.metadata.name==resp_ing.spec.rules[1].http.paths[0].backend.service_name and resp_deploy.metadata.name==resp_svc.spec.selector['app']


if __name__ == '__main__':
    # create_service()

    # import requests
    # url = "http://192.168.93.121/test-haproxy-ingress"

    #     # x = requests.get(url, headers = {"Host": "test.haproxy.ingress"})
    #     # print(x) # x.status_codeapi
    
    # v1 = client.CoreV1Api()

    # stream = watch.Watch().stream(v1.list_namespaced_pod, label_selector="app=test-haproxy-ingress", namespace="default")
    # for event in stream:
    #     if event['object'].status.phase == 'Running':
    #         x = requests.get(url, headers = {"Host": "test.haproxy.ingress"})
    #         print("Response: {}".format(x))
    #         if x.status_code == '503':
    #             print(x)
    #             continue
    #         else:
    #             print(x)
    #             break

    # soup = BeautifulSoup(x.text, features="lxml")
    # print("Title: {}".format(soup.head.title.text))

    config.load_kube_config('/home/aragorn/SKA/code/glr-kubeconfig')
    print(config.kube_config.Configuration().host)