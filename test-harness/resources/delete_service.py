# Copyright 2016 The Kubernetes Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from os import path
import time
import yaml

from kubernetes import client, config, watch


def delete_service(namespace):
    # Configs can be set in Configuration class directly or using helper
    # utility. If no argument provided, the config will be loaded from
    # default location.
    config.load_kube_config()

    with open(path.join(path.dirname(__file__), "nginx.yaml")) as f:
        dep = yaml.safe_load(f)
        deploy_name = dep['metadata']['name']
        k8s_apps_v1 = client.AppsV1Api()
        resp_deploy = k8s_apps_v1.delete_namespaced_deployment(
            name=deploy_name, namespace=namespace)

    with open(path.join(path.dirname(__file__), "nginx_svc.yaml")) as f:
        svc = yaml.safe_load(f)
        svc_name = svc['metadata']['name']
        k8s_core_v1 = client.CoreV1Api()
        resp = k8s_core_v1.delete_namespaced_service(
            name=svc_name, namespace=namespace
        )
        
    with open(path.join(path.dirname(__file__), "ingress.yaml")) as f:
        ing = yaml.safe_load(f)
        ing_name = ing['metadata']['name']
        k8s_v1_beta1 = client.NetworkingV1beta1Api()
        resp_ing = k8s_v1_beta1.delete_namespaced_ingress(namespace=namespace, name=ing_name)
    
    
    v1 = client.CoreV1Api()
    deleting = True
    snoozebutton = 1
    while deleting:
        resp = v1.list_namespaced_pod(label_selector="app=test-haproxy-ingress", namespace=namespace)
        deleting = (len(resp.items) != 0)
        time.sleep(snoozebutton)
        snoozebutton += 2

    return not deleting

if __name__ == '__main__':
    namespace = "default"
    delete_service(namespace)