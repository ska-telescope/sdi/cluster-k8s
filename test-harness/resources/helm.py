import logging
import os
import string
import subprocess

class HelmAdapter(object):
    def __init__(self, test_namespace):
        self.namespace = test_namespace

    def install(self, instance_name, chart_url, cmd_args="", set_flag_values={}):
        set_flag = ''
        if set_flag_values:
            set_flag = self.create_set_cli_flag_from(set_flag_values)

        cmd = f"helm install {instance_name} {chart_url} --namespace={self.namespace} {cmd_args} {set_flag}"
        return self._run_subprocess(cmd.split())

    def delete(self, instance_name):
        cmd = f"helm uninstall {instance_name} --namespace={self.namespace}"
        return self._run_subprocess(cmd.split())

    @staticmethod
    def _run_subprocess(shell_cmd):
        assert isinstance(shell_cmd, list)
        try:
            result = subprocess.run(shell_cmd, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE, encoding="utf8", check=True)
        except subprocess.CalledProcessError as e:
            logging.error("!!! Ran: %s", " ".join(e.cmd))
            logging.error("!!! Got stdout: %s", e.stdout)
            logging.error("!!! Got stderr: %s", e.stderr)
            raise
        return result.stdout

    @staticmethod
    def create_set_cli_flag_from(values):
        chart_values = [f"{key}={value}" for key, value in values.items()]
        set_flag = "--set={}".format(",".join(chart_values))
        return set_flag
