import logging
import os
import string
import subprocess

class EtcdCtlAdapter(object):
    def __init__(self, endpoints, ca_crt, healthcheck_client_crt, healthcheck_client_key):
        self.endpoints = endpoints
        self.ca_crt = ca_crt
        self.healthcheck_client_crt = healthcheck_client_crt
        self.healthcheck_client_key = healthcheck_client_key

    def list_keys(self):
        os.environ["ETCDCTL_API"] = "3"
        cmd = f"/usr/bin/etcdctl --endpoints {self.endpoints} --cacert={self.ca_crt} --cert={self.healthcheck_client_crt} --key={self.healthcheck_client_key} get / --prefix --keys-only"
        return self._run_subprocess(cmd.split())

    def get_key(self, key):
        os.environ["ETCDCTL_API"] = "3"
        cmd = f"/usr/bin/etcdctl --endpoints {self.endpoints} --cacert={self.ca_crt} --cert={self.healthcheck_client_crt} --key={self.healthcheck_client_key} get --from-key {key}"
        return self._run_subprocess(cmd.split())

    @staticmethod
    def _run_subprocess(shell_cmd):
        assert isinstance(shell_cmd, list)
        try:
            result = subprocess.run(shell_cmd, stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            logging.error("!!! Ran: %s", " ".join(e.cmd))
            logging.error("!!! Got stdout: %s", e.stdout)
            logging.error("!!! Got stderr: %s", e.stderr)
            raise
        return result.stdout