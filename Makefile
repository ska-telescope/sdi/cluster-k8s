INVENTORY_FILE ?= ./inventory_k8s##inventory file to be generated
PRIVATE_VARS ?= ./centos_vars.yml##template variable for heat-cluster
ANSIBLE_USER ?= centos## ansible user for the playbooks
CLUSTER_KEYPAIR ?= piers-engage## key pair available on openstack to be put in the created VMs
DEBUG ?= false
CONTAINERD ?= true
NVIDIA ?= false
IGNORE_NVIDIA_FAIL ?= false
TAGS ?= resources
EXTRA_VARS ?=
COLLECTIONS_PATHS ?= ./collections
V ?=

.DEFAULT_GOAL := help

.PHONY: check_production

# define overides for above variables in here
-include PrivateRules.mak

ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska/systems_stack_cluster/playbooks
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska/systems_docker_base/playbooks
K8S_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska/systems_k8s/playbooks

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"
	@echo "CONTAINERD=$(CONTAINERD)"
	@echo "NVIDIA=$(NVIDIA)"
	@echo "IGNORE_NVIDIA_FAIL=$(IGNORE_NVIDIA_FAIL)"
	@echo "DEBUG=$(DEBUG)"

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/*

install:  ## Install dependent ansible collections
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections

reinstall: uninstall install ## reinstall collections

all: build

check_production:
	(grep 'production: true' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep "k8s_system") ) || \
	(grep 'production: false' $(PRIVATE_VARS) && (echo "$(INVENTORY_FILE)" | grep -v "k8s_system") ) || \
	(printf "aborting, as this is not production ($(PRIVATE_VARS)) \n"; exit 1)

build_vms: check_production ## Build vms only based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)" $(V)

build_nodes: check_production ## Build nodes based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="cluster_keypair=$(CLUSTER_KEYPAIR)" \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)" $(V)
	make build_common
	make build_docker

build_common:  ## apply the common roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(PRIVATE_VARS) $(V)

build_docker:  ## apply the docker roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(PRIVATE_VARS) $(V)

clean_nodes:  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/remove-cluster-infra.yml $(V)

clean: clean_nodes  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!

check_nodes: check_production ## Check nodes based on heat-cluster
	ansible -i ./$(INVENTORY_FILE) cluster \
	                         -m shell -a 'df; lsblk'

lint: reinstall ## Lint check playbook
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint -p playbooks/reset.yml \
	             playbooks/test.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
				 $(DOCKER_PLAYBOOKS)/docker.yml \
				 $(K8S_PLAYBOOKS)/charts.yml \
				 $(K8S_PLAYBOOKS)/loadbalancers.yml \
				 $(K8S_PLAYBOOKS)/setup.yml $(V) > ansible-lint-results.txt; \
	cat ansible-lint-results.txt

build_k8s: check_production ## Build k8s
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/setup.yml \
	-e @$(PRIVATE_VARS) \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

apply_resource_quotas:
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/resources.yml \
	-e @$(PRIVATE_VARS) \
	--tags $(TAGS) \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

build: build_nodes build_haproxy build_k8s  ## Build nodes, haproxy and k8s

build_charts: check_production ## Build charts
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/charts.yml \
	-e @$(PRIVATE_VARS) \
	--extra-vars="activate_containerd=$(CONTAINERD) activate_nvidia=$(NVIDIA) ignore_nvidia_fail=$(IGNORE_NVIDIA_FAIL) debug=$(DEBUG)" $(V)

build_haproxy: check_production ## Build haproxy
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -i $(INVENTORY_FILE) $(K8S_PLAYBOOKS)/loadbalancers.yml \
	  -e @$(PRIVATE_VARS) $(V)

clean_k8s: check_production ## clean k8s cluster
	ansible-playbook -i $(INVENTORY_FILE) playbooks/reset.yml -e @$(PRIVATE_VARS) $(V)

test: ## Smoke test for new created cluster
	ansible-playbook -i $(INVENTORY_FILE) playbooks/test.yml -e @$(PRIVATE_VARS) -vvv

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
