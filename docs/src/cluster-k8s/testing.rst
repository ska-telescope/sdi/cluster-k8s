.. doctest-skip-all
.. _package-guide:

.. todo::
    - Add X-ray reports

**************************
BDD Testing of cluster-k8s
**************************

The SKA follows a BDD pattern wherever it is applicable. This kubernetes cluster provisioning repository includes a few Gherkin tests that enable an interface between python code written by developers (using pytest) and natural language scenarios that can be written by Business Owners, Product Owners, Testers and the like, using `pytest-bdd <https://pypi.org/project/pytest-bdd/>`_.

Testing-harness
===============

The following files and directories are relevant at the time of writing (this may not be kept up to date):

.. code-block:: bash
  :linenos:

    ../test-harness/
    ├── Makefile
    ├── README.md
    ├── cucumber.json
    ├── features
    │   ├── auth.feature
    │   ├── ceph.feature
    │   ├── cluster.feature
    │   ├── dns.feature
    │   ├── etcd.feature
    │   ├── networking.feature
    │   ├── resources.feature
    │   ├── sandbox.feature
    │   └── skampi.feature
    ├── resources
    ...
    │   └── skampi_values.json
    ├── setup.cfg
    ├── test-requirements.txt
    └── tests
        ├── conftest.py
        ├── test_auth.py
        ├── test_ceph.py
        ├── test_cluster.py
        ├── test_dns.py
        ├── test_etcd.py
        ├── test_networking.py
        ├── test_resources.py
        └── test_skampi.py

Refer to the line numbers. Each feature, with name ``<feature_name>``, has a corresponding ``<feature_name>.feature`` file under the ``features/`` directory, and a ``test_<feature_name>.py`` under the ``/tests/`` directory.

At the bottom of the python file where the test functions are developed, we include a line that links the feature file to the test file. As an example, the skampi feature is described here:

The feature is written in Gherkin language:

.. code-block:: bash

    <features/skampi.feature>

    Feature: SKAMPI
        Install skampi and smoke test it

    Scenario: Install skampi and smoke test it
        Given a Kubernetes cluster with KUBECONFIG .kube/config
        When I install the chart at stable/etcd-operator with name etcd0 with values file resources/etcd_values.json
        And I install the chart at https://gitlab.com/ska-telescope/skampi/-/raw/master/repository/skampi-0.1.0.tgz with name test2 with values file resources/skampi_values.json
        Then after 10 minutes all pods are running

We will not include the full file here, but under the ``/tests/`` directory there is a corresponding python file named ``test_skampi.py``. The ``test_`` prefix ensures that pytest picks it up as part of the test suite.

The last line in this file is of importance:

.. code-block::

    scenarios('../features/skampi.feature')

This pulls the scenarios from the .feature file and links each test function to a corresponding line in the Scenario description.

Additional and helper code is added to the ``resources/`` directory.
